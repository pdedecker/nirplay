# Nirplay

![Nirplay](docs/tablet.png)

Single-page application to listen to [VRT](https://www.vrt.be/) radio stations. Powered by [Vue.js](https://vuejs.org/), [Webpack](https://webpack.js.org/), the [Vuetify](https://vuetifyjs.com/) UI framework and [Vuex](https://vuex.vuejs.org/).

The name is derived from [NIR](https://nl.wikipedia.org/wiki/Nationaal_Instituut_voor_de_Radio-omroep), the Belgian public broadcaster that preceeded the VRT.

## FAQ
### What does it do?
This repository contains an unofficial radio player for VRT stations. It compiles to a static website. Instead of requiring a backend or hardcoding a list of VRT stations with their streaming URLs, it leverages VRT's radio API to populate a media player with its current lineup of radio stations.

### Can I try it?
You absolutely can! Scroll down for setup instructions.

### Okay, but can't you just make it available as a website?
At this point, the VRT does not want external parties to use their radio API. This includes non-commercial usage. I respect their wish and therefore won't provide a public website here.

### Any new features you'd like to add in the future?
Lots! I'd like to integrate playlist data, album art and a visual for each show, which would require different approaches for mobile and desktop screens to do it well. I think it'd be cool to provide additional usability improvements like a "rewind 30 seconds" button. Plus, I'd like to plug in to the VRT's catalog of previously aired shows and make those available too. Although this is all technically feasible thanks to their comprehensive API, much of the fun of making stuff is that other people will get to use it. Because of reasons mentioned earlier, new features are on hold.

### Do you have a suite of unit tests?
It's on my list! I was thinking of using [Karma](https://karma-runner.github.io/) as the test runner and [Jasmine](https://jasmine.github.io/) as the test framework. But as said, development is on hold.

## License and other sleep-inducing legal stuff
Anyone is free to take a look at the files in this repository. If you'd like to incorporate parts of my work into an application of your own, you do need my permission. Contact me at nirplay@pieterdedecker.be if you'd like to discuss the possibilities.

Nirplay is not affiliated with the VRT. It plugs in to an openly available VRT API so it can show a list of radio stations and play them. The intellectual property rights on assets loaded via their API belong to their respective owners. Nirplay does not store any brand logos, visuals or other VRT-owned assets within its repository - except in the form of app screenshots.

## Setup
### Installing dependencies
```
yarn install
```

### Start a development server with hot reloading
```
yarn run serve
```

### Compile a minified production build
```
yarn run build
```

After running the command, the `/dist` folder will contain the build artifacts.

### Lint and fix files
```
yarn run lint
```
