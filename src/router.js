import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: "Luister live",
      },
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        title: "Over",
      },
    }
  ]
})

// Custom directive hooking in to v-page-title on <router-view> so that we can update the document title
// every time the user navigates to a different page.
//
// Idea sourced from https://github.com/vuejs/vue-router/issues/914#issuecomment-326536513.
Vue.directive('page-title', {
  inserted: (el, binding) => document.title = binding.value,
  update: (el, binding) => document.title = binding.value,
})
