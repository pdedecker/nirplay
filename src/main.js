import '@babel/polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// Initialize global application state
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    activeLiveChannel: null
  },
  mutations: {
    // Call this to cue a new livestream. The player should watch this event and begin playback.
    setLiveChannel (state, channelId) {
      state.activeLiveChannel = channelId
    },
    // Request that media playback be stopped. The player should hook in to this event and stop playback.
    stop (state) {
      state.activeLiveChannel = null
    },
  }
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
