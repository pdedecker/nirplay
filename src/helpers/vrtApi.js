/**
* Performs an asynchronous call to the VRT services API.
* @param {str} endpoint: what API call to do
* @param {str} spec_version: VRT's API versions specification changes for backwards compatibility
*                            and asks that callers explicitly state the specification they adhere to.
*/
function doVrtApiCall(endpoint, spec_version) {
    return fetch('https://services.vrt.be/' + endpoint, {
        headers: {
            'Accept': 'application/vnd.channel.vrt.be.' + spec_version + '+json'
        }
    }).then(
        // Asynchronously decode JSON response
        response => response.json()
    )
}

/**
 * Appends a pseudorandom cache busting parameter to a given URL.
 * 
 * Due to the combination of VRT's cache headers and the behavior of HTML5 audio elements, we need a
 * unique(ish) stream URL every time to bypass browser cache. This prevents strange glitches when switching
 * from stream A to B and back.
 *
 * @param {str} rawUrl
 * @returns str
 */
function addUniqueParameterToUrl(rawUrl) {
    const url = new URL(rawUrl)
    url.searchParams.set('nocache', Math.random())
    return url.toString()
}

export default {
    /**
     * Asynchronously retrieves a processed list of all active VRT channels.
     */
    getChannels: function () {
        // Sometimes VRT puts dead radio channels in their API with status active. Unclear why.
        // This prevents them from ending up in the UI.
        const channel_id_blacklist = [
            // Item titled "VRT" with no streaming links
            '81',

            // VRT Event: sometimes provides special coverage. Usually silent or a Studio Brussel
            // simulcast.
            '71',  
        ]

        return doVrtApiCall(
            'channel/s/', 'channels_1.1'
        ).then(
            decoded_json => decoded_json['channels'].filter(
                // Filters out TV channels and streams that currently aren't broadcasting.
                item => item.state == 'active' && item.type == 'radio' && !channel_id_blacklist.includes(item.code)
            ).map(
                // Extract just the bits we need to render the channel
                raw_channel => ({
                    id: raw_channel.code,
                    name: raw_channel.displayName,
                    description: raw_channel.description,
                    logoUrl: raw_channel.logoUrl,
                })
            )
        )
    },
    /**
     * Retrieves a playable stream for a given VRT channel.
     */
    getStreamForChannel: function (channelId) {
        return doVrtApiCall(
            'channel/s/' + channelId + '/streams', 'streams_1.0'
        ).then(function (decoded_json) {
            let mp3_stream_url = null
            for (const stream of decoded_json.streams) {
                // Aim for a 128kbps AAC+ stream, because it's the best quality available.
                if (stream.code == 'aac_128') {
                    return addUniqueParameterToUrl(stream.url)
                }
                // Keep the 128kbps MP3 stream as a backup when we encounter it, because normally it's available for all channels.
                else if (stream.code == 'mp3_128') {
                    mp3_stream_url = stream.url
                }
            }

            // No AAC+ stream found? Return the backup stream URL if available.
            return addUniqueParameterToUrl(mp3_stream_url)
        })
    }
}